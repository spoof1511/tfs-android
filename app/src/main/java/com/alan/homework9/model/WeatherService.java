package com.alan.homework9.model;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherService {

    public Single<Weather> getWeatherResult(String cityName) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .addNetworkInterceptor(loggingInterceptor)
                .build();
        Gson gson = new GsonBuilder().registerTypeAdapter(Weather.class, new WeatherDeserializer()).create();
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/data/2.5/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client);

        WeatherApi api = builder.build().create(WeatherApi.class);

        String key = "8d8892df303eee319486530ee434dc66";
        String metric = "metric";
        return api.getWeather(cityName, key, metric);
    }

    private class WeatherDeserializer implements JsonDeserializer<Weather> {
        @Override
        public Weather deserialize(JsonElement json, Type typeOfT,
                                   JsonDeserializationContext context) throws JsonParseException {
            JsonObject jsonObject = json.getAsJsonObject();
            JsonObject mainJsonObject = jsonObject.getAsJsonObject("main");
            JsonObject windJsonObject = jsonObject.getAsJsonObject("wind");
            JsonArray weatherJsonArray = jsonObject.getAsJsonArray("weather");

            String city = jsonObject.get("name").getAsString();
            float temp = mainJsonObject.get("temp").getAsFloat();
            float windSpeed = windJsonObject.get("speed").getAsFloat();

            JsonObject weatherObject = weatherJsonArray.get(0).getAsJsonObject();
            String name = weatherObject.get("main").getAsString();
            String description = weatherObject.get("description").getAsString();
            return new Weather(name, description, city, temp, windSpeed);
        }
    }
}
