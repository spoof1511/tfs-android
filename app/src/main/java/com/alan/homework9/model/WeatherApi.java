package com.alan.homework9.model;


import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherApi {
    @GET("weather")
    Single<Weather> getWeather(@Query("q") String params,
                               @Query("APPID") String countryCode,
                               @Query("units") String metric);
}
